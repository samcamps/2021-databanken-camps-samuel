use ApDB;

CREATE VIEW AuteursBoekenRatings AS
     SELECT 
       Auteur, Titel, Rating
     FROM
         AuteursBoeken
             INNER JOIN
         GemiddeldeRatings ON GemiddeldeRatings.Boeken_Id = AuteursBoeken.Boeken_Id;