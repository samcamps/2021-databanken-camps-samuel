USE `aptunes`;
DROP procedure IF EXISTS `NumberOfGenres`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `NumberOfGenres` (OUT aantal TINYINT)
BEGIN
	SELECT count(*)
    from Genres
    INTO aantal;
END$$

DELIMITER ;