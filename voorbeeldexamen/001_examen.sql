use aptunes_examen;

SELECT 
    Albums.Titel AS 'Album', Bands.Naam AS 'Band'
FROM
    Albumreleases
        INNER JOIN
    Albums ON Albums.Id = Albumreleases.Albums_Id
        INNER JOIN
    Bands ON Bands.Id = Albumreleases.Bands_Id;

