USE `aptunes`;
DROP procedure IF EXISTS `MockalbumreleasesLoop`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `MockalbumreleasesLoop` (IN extraReleases INT)
BEGIN
 DECLARE counter INT default 0;
    
aanmaakloop: LOOP
		CALL MockAlbumReleaseWithSuccess(@gelukt);
		IF @gelukt = 1 THEN 
			SET counter = counter +1;
        END IF;    
            
		IF counter = extraReleases THEN
			LEAVE aanmaakloop;
		END IF;
    
    END LOOP aanmaakloop;
END$$

DELIMITER ;