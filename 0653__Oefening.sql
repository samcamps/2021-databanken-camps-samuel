USE `aptunes`;
DROP procedure IF EXISTS `DangerousInsertAlbumreleases2`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `DangerousInsertAlbumreleases2` ()
SQL SECURITY INVOKER
BEGIN
	DECLARE numberOfAlbums INT default 0 ;
    DECLARE	numberOfBands INT default 0;
    DECLARE randomAlbumId INT default 0;
    DECLARE randomBandId INT default 0;
	DECLARE teller INT DEFAULT 0;
    
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
		SELECT 'Nieuwe releases konden niet worden toegevoegd';
		ROLLBACK;
	END;

	SELECT COUNT(*) from Albums
    INTO numberOfAlbums;
    
    SELECT COUNT(*) from Bands
    INTO numberOfBands;
    
    START TRANSACTION;
    
    REPEAT
    
		SET randomAlbumId = FLOOR (RAND() * numberOfAlbums +1);
		SET randomBandId = FLOOR (RAND() * numberOfBands +1);

		INSERT INTO Albumreleases (Bands_Id, Albums_Id)
		Values (randomBandId, randomAlbumId);
		
        IF teller = 2 AND FLOOR (RAND()*3)+1 = 1 THEN
			SIGNAL SQLSTATE '45000';
        END IF;
        
        SET teller = teller +1;	 
        
		UNTIL teller = 3
    
    END REPEAT;
	
    COMMIT;
    
END$$

DELIMITER ;

