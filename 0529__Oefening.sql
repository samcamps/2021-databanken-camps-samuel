use ApDB;

select Studenten_Id, avg(Cijfer)
from Evaluaties
group by Studenten_Id
having avg(Cijfer) > (select avg(Cijfer) from Evaluaties);