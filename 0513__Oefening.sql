use ApDB;
CREATE VIEW AuteursBoeken AS
    SELECT 
        CONCAT(Personen.Voornaam,
                ' ',
                Personen.Familienaam) AS Auteur,
        Boeken.Titel
    FROM
        Publicaties
            INNER JOIN
        Boeken ON Boeken_Id = Boeken.Id
            INNER JOIN
        Personen ON Personen_Id = Personen.Id;