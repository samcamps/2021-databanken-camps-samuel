use ApDB;
SELECT 
    
    COALESCE(Leden.Voornaam, 'Taak niet toegewezen') AS Voornaam,
    Taken.Omschrijving
FROM
    Taken
        LEFT JOIN
    Leden ON Leden_Id = Leden.Id;
