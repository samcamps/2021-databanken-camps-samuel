Alter view AuteursBoekenRatingsEnkelQuery
as
select CONCAT(Personen.Voornaam,' ', Personen.Familienaam) AS Auteur,Boeken.Titel, AVG(Reviews.Rating) as Rating
 FROM
 Publicaties
 INNER JOIN
 Boeken ON Boeken_Id = Boeken.Id
 INNER JOIN
 Personen ON Personen_Id = Personen.Id
 INNER JOIN
 Reviews on Boeken.Id = Reviews.Boeken_Id
 Group By Auteur, Titel;
