USE `aptunes`;
DROP procedure IF EXISTS `AltWelcome`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `AltWelcome` ()
BEGIN
	DECLARE lijst VARCHAR(200) DEFAULT'';
    DECLARE temp VARCHAR(200);
    DECLARE teller INT DEFAULT 0;
	DECLARE top CURSOR FOR
	
			select Bands.Naam from Liedjes
			inner join Bands
			on Bands_Id = Bands.Id
			group by Bands.Naam
			order by COUNT(*) DESC
			LIMIT 3;

	OPEN top;
	
    leesloop: LOOP
		IF teller = 3 THEN 
			SET lijst = SUBSTRING(lijst,1, length(lijst)-2);
			LEAVE leesloop;
		END IF;
        
		FETCH top INTO temp;
		SET lijst = CONCAT(temp,', ',lijst); 
        SET teller = teller + 1;
    
     END LOOP leesloop;
     
    CLOSE top;
        
	SELECT CONCAT('Welkom bij APTunes! Wij hebben de nieuwste nummers van ', lijst) as Message;

END$$

DELIMITER ;

