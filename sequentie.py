# INSTRUCTIES:
# zorg dat je Python3 op je systeem hebt (te downloaden op python.org)
# let er bij installatie op dat Python3 op je pad wordt geplaatst (dit is een checkbox)
# installeer achteraf ook nodige packages met het commando: pip install --user regex click mysql-connector-python
# plaats dit script in de map met je eigen scripts
# voer dan dit script uit met het laagste en hoogste nummer voor een script dat je wil uitvoeren
# dit doe je bijvoorbeeld (nadat je een terminal hebt geopend in de map met je scripts) met: python3 sequentie.py 1 12
# dit voert scripts 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 en 12 in volgorde uit
# je hoeft geen nullen voor het nummer van het script te plaatsen
import os
import regex
import click
import mysql.connector

@click.command()
@click.argument('least',required=True)
@click.argument('greatest',required=True)
def runscripts(least,greatest):
    patt = regex.compile(r'(?<number>[0-9]+)_.+\.sql.*')
    scripts = sorted([e for e in os.listdir('.')
                     if patt.fullmatch(e)
                     and int(least) <= int(patt.fullmatch(e).group(1)) <= int(greatest)])
    cnx = mysql.connector.connect(user='databanken',
                                  password='databanken',
                                  host='localhost',
                                  port='3310',
                                  autocommit=True)
    cursor = cnx.cursor(buffered=True)
    for script in scripts:
        with open(script) as fh:
            contents = fh.read()
        for _ in cursor.execute(contents,multi=True):
            pass
    cursor.close()
    cnx.close()

if __name__ == '__main__':
    runscripts()
