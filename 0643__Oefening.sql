USE `aptunes`;
DROP procedure IF EXISTS `CreateAndReleaseAlbum`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `CreateAndReleaseAlbum` (IN titel VARCHAR(100), IN bands_id INT)
BEGIN
	INSERT INTO Albums (Titel)
    Values (titel);
    
    INSERT INTO Albumreleases (Bands_Id, Albums_Id)
    Values (bands_id, last_insert_id());
    
END$$

DELIMITER ;
