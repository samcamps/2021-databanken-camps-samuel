USE `aptunes`;
DROP procedure IF EXISTS `Welcome`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `Welcome` ()
BEGIN
	
    SET @lijst = '';
	CALL VoorbeeldCursors(@lijst);
    SELECT CONCAT('Welkom bij ApTunes! Wij hebben ', @lijst) as Message;
    
END$$

DELIMITER ;