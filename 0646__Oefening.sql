USE `aptunes`;
DROP procedure IF EXISTS `MockAlbumreleases`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `MockAlbumreleases`(IN extraReleases INT)
BEGIN

	DECLARE counter INT default 0;
    
    REPEAT
		CALL MockAlbumReleaseWithSuccess(@gelukt);
		IF @gelukt = 1 THEN 
			SET counter = counter +1;
        END IF;    
            
    UNTIL counter >= extraReleases
    
    END REPEAT;

END$$

DELIMITER ;