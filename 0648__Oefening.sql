USE `aptunes`;
DROP procedure IF EXISTS `DemonstrateHandlerOrder`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `DemonstrateHandlerOrder` ()
BEGIN
	DECLARE randomgetal INT;
    
    DECLARE continue handler for SQLSTATE '45002'
	SELECT 'State 45002 opgevangen. Geen probleem.';
    
    DECLARE continue handler for SQLEXCEPTION
	SELECT 'Een algemene fout opgevangen';
    
    
    SET randomgetal = FLOOR((RAND() * 3) + 1);
   
    IF randomgetal = 1 THEN
		SIGNAL SQLSTATE '45001';
    ELSEIF randomgetal = 2 THEN
		SIGNAL SQLSTATE '45002';
	ELSE 
		SIGNAL SQLSTATE '45003';
			
	END IF;

END$$

DELIMITER ;

