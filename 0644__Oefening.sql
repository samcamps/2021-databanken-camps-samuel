USE `aptunes`;
DROP procedure IF EXISTS `MockAlbumRelease`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `MockAlbumRelease`()
BEGIN
	declare numberOfAlbums INT default 0 ;
    declare	numberOfBands INT default 0;
    declare randomAlbumId INT default 0;
    declare randomBandId INT default 0;

	SELECT COUNT(*) from Albums
    INTO numberOfAlbums;
    
    SELECT COUNT(*) from Bands
    INTO numberOfBands;
    
    SET randomAlbumId = FLOOR (RAND() * numberOfAlbums +1);
    SET randomBandId = FLOOR (RAND() * numberOfBands +1);
    
    IF randomAlbumId NOT IN (Select Albums_Id from Albumreleases WHERE Bands_Id = randomBandId AND Albums_Id = randomAlbumId) THEN
     INSERT INTO Albumreleases (Bands_Id, Albums_Id)
     Values (randomBandId, randomAlbumId);
         
    END IF;


END$$

DELIMITER ;