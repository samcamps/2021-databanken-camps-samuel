-- Zoek, volgens de uitleg in de cursus de ideale prefixlengte voor de kolommen Voornaam en Familienaam van de tabel Muzikanten. 
-- Noteer beide voor jezelf. Maak dan opnieuw een index op de combinatie van Voornaam en Familienaam, ieder met de ideale prefixlengte. 
-- 9

-- select distinct left (Familienaam, 9)
-- from Muzikanten;

create index VoornaamFamilienaam_Idx
on Muzikanten (Voornaam(9), Familienaam(9));