USE `aptunes`;
DROP procedure IF EXISTS `GetLiedjes`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`databanken`@`%` PROCEDURE `GetLiedjes`(IN tekst VARchar(50))
BEGIN
 SELECT Titel
 FROM Liedjes
 WHERE Titel LIKE CONCAT('%',tekst,'%');
END$$

DELIMITER ;

