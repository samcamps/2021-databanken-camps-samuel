USE `aptunes`;
DROP procedure IF EXISTS `CleanupOldMemberships`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `CleanupOldMemberships` (IN datum DATE, OUT verwijderd INT)
BEGIN
	SELECT count(*)
    FROM Lidmaatschappen
    WHERE Einddatum < datum
    INTO verwijderd;
    
    SET SQL_SAFE_UPDATES = 0;
    DELETE
    FROM Lidmaatschappen
    WHERE Einddatum < datum;
    SET SQL_SAFE_UPDATES = 1;

END$$

DELIMITER ;


