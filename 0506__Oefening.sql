use ApDB;

SELECT 
    Voornaam,
    Titel,
    Startdatum,
    COALESCE(Einddatum, 'nog niet teruggebracht') AS Einddatum
FROM
    Uitleningen
        INNER JOIN
    Leden ON Uitleningen.Leden_Id = Leden.Id
        INNER JOIN
    Boeken ON Uitleningen.Boeken_Id = Boeken.Id;