use ApDB;

Alter view AuteursBoeken
as
select CONCAT(Personen.Voornaam,' ', Personen.Familienaam) AS Auteur,Boeken.Titel, Boeken.Id as Boeken_Id
 FROM
        Publicaties
            INNER JOIN
        Boeken ON Boeken_Id = Boeken.Id
            INNER JOIN
        Personen ON Personen_Id = Personen.Id;
