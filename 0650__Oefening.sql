USE `aptunes`;
DROP procedure IF EXISTS `VoorbeeldCursors`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `VoorbeeldCursors` (
    INOUT inoutGenresList VARCHAR(1000))
BEGIN
  DECLARE ok INTEGER DEFAULT 0;
  DECLARE genre VARCHAR(50) DEFAULT "";

  DECLARE currentGenre
  CURSOR FOR SELECT Naam FROM Genres;

  DECLARE CONTINUE HANDLER
  FOR NOT FOUND SET ok = 1;

  OPEN currentGenre;

  getGenre: LOOP
    FETCH currentGenre INTO genre;
	
    IF ok = 1 THEN
       SET inoutGenresList = SUBSTRING(inoutGenresList, 1, LENGTH(inoutGenresList)-1);
       LEAVE getGenre;
	END IF;
    
    SET inoutGenresList = CONCAT(genre,',',inoutGenresList);
    END LOOP getGenre;

  CLOSE currentGenre;
END$$

DELIMITER ;