use ApDB;

select Games.Titel, coalesce( Platformen.Naam,"Platform niet meer ondersteund") as Naam 
from Games
left join Releases
on Games.Id = Games_Id
left join Platformen
on Platformen.Id = Platformen_Id;